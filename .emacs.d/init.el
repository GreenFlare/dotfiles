
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(load "~/.emacs.d/packages.el")
(load "~/.emacs.d/local-packages.el")
(load "~/.emacs.d/functions.el")
(load "~/.emacs.d/options.el")
(load "~/.emacs.d/mu4e.el")
(load "~/.emacs.d/org.el")
